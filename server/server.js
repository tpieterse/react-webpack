/* eslint-disable no-console */
const express = require('express'); //used for routing
const cors = require('cors');
var bodyParser = require('body-parser');

const routes = require('./routes/index');
const config = require('../server-config');

const app = express();

app.use(express.static('public'));

app.use(cors());
app.use('/', routes);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(function (req, res, next) {
    res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
    res.header('Expires', '-1');
    res.header('Pragma', 'no-cache');
    next()
});

app.listen(config.server.port, () => console.log(`Image-Server listening on port ${config.server.port}`));
