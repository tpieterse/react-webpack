const express = require('express');
const slideController = require('../controllers/example.ctrl');

const router = express.Router();

router.get('/example', slideController.getAll);

module.exports = router;
