# react-webpack
A boilerplate for React and Node configured with Babel and Webpack.

## installation
`npm install`

## run node server
`node server/server.js`

## start
`npm start`

## run tests
`npm test`

## defects