import React, { Component } from 'react';

import service from '../services/service';

class Example extends Component {
    constructor(props) {
      super(props);
      this.state = { 
        greeting: 'Hello world',
        text: 'zz'
      };
    }

    componentDidMount () {
      service.getAll().then(res => this.setState({ text: res.data.text, }));
    }
    
    render() {
      return (
        <div>
          <h1 className="sub-heading">{this.state.greeting}</h1>
          <p>{this.state.text}</p>
        </div>
      );
    }
}

export default Example;
