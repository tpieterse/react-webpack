import React, { Component } from 'react';

import Example from './components/Example';

class App extends Component {
  render() {
    return (
      <div className="container">
        <header className="header">
          <h1 className="main-heading">Boilerplate</h1>
        </header>
        <Example />
      </div>
    );
  }
}

export default App;
