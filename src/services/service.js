const axios = require('axios');
const config = require('./services-config');

const API = config.api.baseUrl;

var service = {
    getAll: function () {
        return axios.get(API + '/example')
            .then(function (data) {
                return data;
            })
    }
}

module.exports = service;
