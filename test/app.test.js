import React from 'react';
import { mount, shallow } from 'enzyme';
import {expect} from 'chai';

import App from '../src/App';

describe('<App/>', function () {
  it('should contain a main heading', function () {
    const wrapper = shallow(<App/>);
    expect(wrapper.find('.main-heading')).to.have.length(1);
  });
});
