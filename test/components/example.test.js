import React from 'react';
import { mount, shallow } from 'enzyme';
import {expect} from 'chai';

import Example from '../../src/components/Example';

describe('<Example/>', function () {
  it('should contain an Example with a sub heading', function () {
    const wrapper = mount(<Example/>);
    expect(wrapper.find('.sub-heading')).to.have.length(1);
  });
});
